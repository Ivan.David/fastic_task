import 'package:fastic_task/step_counter/data/datasources/pedometer_data_source.dart';
import 'package:fastic_task/step_counter/data/datasources/pedometer_local_data_source.dart';
import 'package:fastic_task/step_counter/data/datasources/pedometer_plugin_data_source.dart';
import 'package:fastic_task/step_counter/data/datasources/pedometer_shared_pref_data_source.dart';
import 'package:fastic_task/step_counter/domain/repositories/pedometer_repository.dart';
import 'package:fastic_task/step_counter/domain/repositories/pedometer_repository_impl.dart';
import 'package:fastic_task/step_counter/presentation/bloc/pedometer_alarm_bloc.dart';
import 'package:fastic_task/step_counter/presentation/bloc/pedometer_bloc.dart';
import 'package:get_it/get_it.dart';

// I've used this simple service locator as a way to easily implement
// the interfaces. This is where we would easily change the implementation if,
// a data source would change, for instance.
final serviceLocator = GetIt.instance;

Future<void> init() async {
  _initFeatures();
}

void _initFeatures() {
  // Blocs
  serviceLocator.registerFactory(
      () => PedometerBloc(pedometerRepository: serviceLocator()));
  serviceLocator.registerFactory(
      () => PedometerAlarmBloc(pedometerRepository: serviceLocator()));

  // Repository
  serviceLocator
      .registerLazySingleton<PedometerRepository>(() => PedometerRepositoryImpl(
            pedometerLocalDataSource: serviceLocator(),
            pedometerDataSource: serviceLocator(),
          ));

  // Data sources
  serviceLocator.registerLazySingleton<PedometerDataSource>(
      () => PedometerPluginDataSource());
  serviceLocator.registerLazySingleton<PedometerLocalDataSource>(
      () => PedometerSharedPrefDataSource());
}
