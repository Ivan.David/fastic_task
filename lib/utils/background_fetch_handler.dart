import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';

// I've let those prints in order to be easy to understand what's happening
// if you'll run the app
class BackgroundFetchHandler {
  static Future<void> initPlatformState(
      {@required Function(String) onBackgroundFetch,
      @required Function onError}) async {
    BackgroundFetch.configure(
        BackgroundFetchConfig(
          minimumFetchInterval: 15,
          forceAlarmManager: true,
          stopOnTerminate: false,
          startOnBoot: true,
          enableHeadless: true,
          requiresBatteryNotLow: false,
          requiresCharging: false,
          requiresStorageNotLow: false,
          requiresDeviceIdle: false,
          requiredNetworkType: NetworkType.NONE,
        ),
        onBackgroundFetch, (String taskId) async {
      // <-- Task timeout handler.
      // This task has exceeded its allowed running-time.  You must stop what you're doing and immediately .finish(taskId)
      print("[BackgroundFetch] TASK TIMEOUT taskId: $taskId");
      BackgroundFetch.finish(taskId);
    }).then((int status) async {
      print('[BackgroundFetch] configure success: $status');
    }).catchError((e) {
      onError(e);
      print('[BackgroundFetch] configure ERROR: $e');
    });
  }

  static Future stopBackgroundFetch() async {
    await BackgroundFetch.stop().then((int status) {
      print('[BackgroundFetch] stop success: $status');
    });
  }

  static Future startBackgroundFetch() async {
    await BackgroundFetch.start().then((int status) {
      print('[BackgroundFetch] start success: $status');
    }).catchError((e) {
      print('[BackgroundFetch] start FAILURE: $e');
    });
  }
}
