class SharedPrefsKeys {
  static final String dailyGoalKey = '_dailyGoal';
  static final String numberOfSteps = '_numberOfSteps';
  static final String isDailyGoalNotificationScheduled =
      '_dailyNotificationScheduled';
}
