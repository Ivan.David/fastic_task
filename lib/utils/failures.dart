import 'package:flutter/material.dart';

abstract class Failure {
  Failure();
}

class ServerFailure extends Failure {}

class StreamFailure extends Failure {
  final String error;

  StreamFailure({@required this.error});
}
