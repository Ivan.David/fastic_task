import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class TimeZoneUtils {
	static tz.TZDateTime nextInstanceOf8PM() {
		final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
		tz.TZDateTime scheduledDate =
		tz.TZDateTime(tz.local, now.year, now.month, now.day, 20);
		if (scheduledDate.isBefore(now)) {
			scheduledDate = scheduledDate.add(const Duration(days: 1));
		}
		return scheduledDate;
	}
}