import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// I have used composition over inheritance since this seems to be the usual way
// this is handled in Flutter and one can add other optional parameters with default values
// as well if they will be needed, and the existing code won't break (color, for instance)

class RoundedButton extends StatelessWidget {
  final VoidCallback onPressed;
  final Widget child;

  const RoundedButton({Key key, @required this.onPressed, this.child})
      : assert(onPressed != null),
        assert(child != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 0,
      hoverElevation: 0,
      focusElevation: 0,
      highlightElevation: 0,
      onPressed: onPressed,
      child: child,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    );
  }
}
