import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// small abstractization over the AppBar in order to hide the back button
// arguably, it can be done in place
class FasticAppBar extends StatelessWidget with PreferredSizeWidget {
  final bool shouldShowBackButton;
  final List<Widget> actions;

  // shouldShowBackButton defaults to true because in an app we'll probably need it more often than not
  // the opposite is true for shouldShowAlarmButton
  const FasticAppBar(
      {Key key, this.shouldShowBackButton = true, this.actions = const []})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        // TODO:- implement me when specs available, should probably be Navigator.of(context).pop();
        onPressed: () => print("I do nothing for now"),
      ),
      actions: actions,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(50.0);
}
