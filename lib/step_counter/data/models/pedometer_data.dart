import 'package:flutter/widgets.dart';

class PedometerData {
  final int steps;
  final double calories;

  // the 0.28 coefficient is taken from the table found here https://www.verywellfit.com/pedometer-steps-to-calories-converter-3882595
  // ideally, a paper researching this should be consulted
  // also, it's close enough to the 0.3 value empirically found by using the real
  // Fastic app for a small walk :D
  PedometerData({@required int steps})
      : steps = steps,
        calories = 0.28 * steps,
        assert(steps != null);
}
