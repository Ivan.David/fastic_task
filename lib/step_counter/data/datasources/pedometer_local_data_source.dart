import 'package:fastic_task/step_counter/data/models/pedometer_data.dart';

abstract class PedometerLocalDataSource {
	Future saveCurrentNoOfSteps(PedometerData data);
	Future<PedometerData> getCurrentNoOfSteps();
	Future saveDailyGoal(int dailyGoal);
	Future<int> getDailyGoal();
}