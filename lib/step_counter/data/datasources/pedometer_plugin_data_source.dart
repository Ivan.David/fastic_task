import 'package:fastic_task/step_counter/data/datasources/pedometer_data_source.dart';
import 'package:fastic_task/step_counter/data/models/pedometer_data.dart';
import 'package:pedometer/pedometer.dart';

// For the record, I kind of don't like this Pedometer plugin because
// you can't choose when the user will see the 'allow motion usage' alert dialog
// so you can't show it at a time relevant for the user, when he/she will be
// more likely to accept
// However, it was easy enough to use for this proof of concept screen :).
class PedometerPluginDataSource implements PedometerDataSource {
  @override
  Stream<PedometerData> getCurrentNoOfSteps() {
    final stepCounterStream = Pedometer.stepCountStream;
    return stepCounterStream
        .map((StepCount stepCount) {
        	print(stepCount);
        	return PedometerData(steps: stepCount.steps);
	});
  }
}
