import 'package:fastic_task/step_counter/data/models/pedometer_data.dart';

// all further connections to this layer must be done through this interface
// because changing this data source at a further moment in time is likely
// and code depending on this must be agnostic to the data source
abstract class PedometerDataSource {
	Stream<PedometerData> getCurrentNoOfSteps();
}