import 'package:fastic_task/step_counter/data/datasources/pedometer_local_data_source.dart';
import 'package:fastic_task/step_counter/data/models/pedometer_data.dart';
import 'package:fastic_task/utils/shared_prefs_keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

// The fastest way to simulate a local database, in my opinion
// Obviously, in a production app the data source would be more advanced (sqlite, hive)
class PedometerSharedPrefDataSource implements PedometerLocalDataSource {
  @override
  Future<PedometerData> getCurrentNoOfSteps() async {
	  SharedPreferences prefs = await SharedPreferences.getInstance();
	  final numberOfSteps = prefs.getInt(SharedPrefsKeys.numberOfSteps);
	  final pedometerData = PedometerData(steps: numberOfSteps);
	  return pedometerData;
  }

  @override
  Future saveCurrentNoOfSteps(PedometerData data) async {
	  SharedPreferences prefs = await SharedPreferences.getInstance();
	  return await prefs.setInt(SharedPrefsKeys.numberOfSteps, data.steps);
  }

  @override
  Future<int> getDailyGoal() async {
	  SharedPreferences prefs = await SharedPreferences.getInstance();
	  final dailyGoal = prefs.getInt(SharedPrefsKeys.dailyGoalKey);
	  return dailyGoal;
  }

  @override
  Future saveDailyGoal(int dailyGoal) async {
	  SharedPreferences prefs = await SharedPreferences.getInstance();
	  return await prefs.setInt(SharedPrefsKeys.dailyGoalKey, dailyGoal);
  }

}