part of 'pedometer_alarm_bloc.dart';

@immutable
abstract class PedometerAlarmEvent {}

class AlarmBellPressed extends PedometerAlarmEvent {}
