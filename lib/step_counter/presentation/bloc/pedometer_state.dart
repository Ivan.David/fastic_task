part of 'pedometer_bloc.dart';

abstract class PedometerState extends Equatable {
  const PedometerState();
}

class PedometerInitial extends PedometerState {
  @override
  List<Object> get props => [];
}

class DailyGoalUpdated extends PedometerState {
  final int dailyGoalSteps;

  DailyGoalUpdated({@required this.dailyGoalSteps})
      : assert(dailyGoalSteps != null);

  @override
  List<Object> get props => [dailyGoalSteps];
}

class Loading extends PedometerState {
  @override
  List<Object> get props => [];
}

class StepDataChanged extends PedometerState {
  final PedometerData data;
  final int dailyGoal;

  StepDataChanged({@required this.data, @required this.dailyGoal})
      : assert(data != null),
        assert(dailyGoal != null);

  @override
  List<Object> get props => [data];
}

class ChangingDailyGoal extends PedometerState {
  @override
  List<Object> get props => [];
}

class PedometerError extends PedometerState {
  final String error;

  PedometerError({@required this.error});

  @override
  List<Object> get props => [error];
}
