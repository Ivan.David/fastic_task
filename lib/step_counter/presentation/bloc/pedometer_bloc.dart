import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastic_task/step_counter/data/models/pedometer_data.dart';
import 'package:fastic_task/step_counter/domain/repositories/pedometer_repository.dart';
import 'package:flutter/material.dart';

part 'pedometer_event.dart';

part 'pedometer_state.dart';

class PedometerBloc extends Bloc<PedometerEvent, PedometerState> {
  final PedometerRepository pedometerRepository;

  StreamSubscription _pedometerSubscription;
  PedometerData pedometerData;
  int dailyGoal;

  PedometerBloc({@required this.pedometerRepository})
      : assert(pedometerRepository != null),
        super(PedometerInitial());

  @override
  Stream<PedometerState> mapEventToState(
    PedometerEvent event,
  ) async* {
    if (event is GetInitialData) {
      _pedometerSubscription?.cancel();

      dailyGoal = await pedometerRepository.getDailyGoal();
      pedometerData = await pedometerRepository.getSavedNumberOfSteps();
      add(PedometerDataChanged(stepsData: pedometerData));
      final failedOrPedometerStream = pedometerRepository.getCurrentNoOfSteps();

      if (failedOrPedometerStream.isLeft()) {
        // is error
        yield PedometerError(error: "Pedometer error");
      }

      final pedometerStream = failedOrPedometerStream.getOrElse(null);
      _pedometerSubscription = pedometerStream.listen((data) {
        pedometerData = data;
        add(PedometerDataChanged(stepsData: pedometerData));
      });
      _pedometerSubscription.onError(
          (error) => add(PedometerDataRetrievalError(error: error.toString())));
    } else if (event is PedometerDataChanged) {
      pedometerData = event.stepsData;
      await pedometerRepository.saveCurrentNoOfSteps(pedometerData);
      yield (StepDataChanged(data: pedometerData, dailyGoal: dailyGoal));
    } else if (event is ChangeDailyGoal) {
      // I think there could be an argument based on the trade off between
      // this bloc being a little bit cluttered or having a separate bloc
      // for the daily goal part of the app
      // I think that the daily goal logic is more easy to follow here and is
      // closely linked to the pedometer data so I think it makes sense
      // to have this inside this bloc.
      yield (ChangingDailyGoal());
    } else if (event is DailyGoalChanged) {
      dailyGoal = event.dailyGoal;
      await pedometerRepository.saveDailyGoal(dailyGoal);
      yield (StepDataChanged(data: pedometerData, dailyGoal: dailyGoal));
    } else if (event is PedometerDataRetrievalError) {
      if (pedometerData != null) {
        // do nothing, keep the last state, maybe it's just a temporary error
        // Definitely we need to log it somewhere
        return;
      }

      yield PedometerError(error: event.error);
    }
  }
}
