part of 'pedometer_bloc.dart';

abstract class PedometerEvent extends Equatable {
  const PedometerEvent();
}

class GetInitialData extends PedometerEvent {
  @override
  List<Object> get props => [];
}

class PedometerDataRetrievalError extends PedometerEvent {
  final String error;

  PedometerDataRetrievalError({@required this.error});

  @override
  List<Object> get props => [error];
}

class PedometerDataChanged extends PedometerEvent {
  final PedometerData stepsData;

  PedometerDataChanged({@required this.stepsData});

  @override
  List<Object> get props => [stepsData];
}

class ChangeDailyGoal extends PedometerEvent {
  @override
  List<Object> get props => [];
}

class DailyGoalChanged extends PedometerEvent {
  final int dailyGoal;

  DailyGoalChanged({@required this.dailyGoal});

  @override
  List<Object> get props => [];
}
