part of 'pedometer_alarm_bloc.dart';

@immutable
abstract class PedometerAlarmState {}

class PedometerAlarmOn extends PedometerAlarmState {}

class PedometerAlarmOff extends PedometerAlarmState {}

class PedometerLoading extends PedometerAlarmState {}

class PedometerAlarmError extends PedometerAlarmState {
  final error;

  PedometerAlarmError({@required this.error});
}
