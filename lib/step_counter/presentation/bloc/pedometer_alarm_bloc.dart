import 'dart:async';

import 'package:background_fetch/background_fetch.dart';
import 'package:bloc/bloc.dart';
import 'package:fastic_task/local_notifications/local_notification_handler.dart';
import 'package:fastic_task/local_notifications/models/notification.dart'
    as customNotif;
import 'package:fastic_task/local_notifications/models/notification_channel.dart';
import 'package:fastic_task/step_counter/domain/repositories/pedometer_repository.dart';
import 'package:fastic_task/utils/background_fetch_handler.dart';
import 'package:fastic_task/utils/shared_prefs_keys.dart';
import 'package:fastic_task/utils/timezone_utils.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'pedometer_alarm_event.dart';

part 'pedometer_alarm_state.dart';

class PedometerAlarmBloc
    extends Bloc<PedometerAlarmEvent, PedometerAlarmState> {
  final PedometerRepository pedometerRepository;

  // arguably could be put as a variable in the onBackgroundFetch, I think it's
  // a matter of taste, I see it as a dependency of the whole Bloc rather than
  // a local variable
  final LocalNotificationHandler localNotificationHandler =
      LocalNotificationHandler();
  bool isAlarmOn = false;

  PedometerAlarmBloc({@required this.pedometerRepository})
      : assert(pedometerRepository != null),
        super(PedometerAlarmOff());

  @override
  Stream<PedometerAlarmState> mapEventToState(
    PedometerAlarmEvent event,
  ) async* {
    if (event is AlarmBellPressed) {
      yield PedometerLoading();
      if (isAlarmOn) {
        isAlarmOn = false;
        await BackgroundFetchHandler.stopBackgroundFetch();
        yield PedometerAlarmOff();
      } else {
        isAlarmOn = true;

        // From the plugin's documentation:
//				Your `callback` Function provided to [configure] will be
//		executed each time a background - fetch event occurs.NOTE the [configure]
//		method automatically calls [start].You do
//		not have to call this method after you first [configure] the plugin.
        BackgroundFetchHandler.initPlatformState(
            onBackgroundFetch: onBackgroundFetch,
            onError: onBackgroundFetchError);
        yield PedometerAlarmOn();
      }
    }
  }

  Stream onBackgroundFetchError(String error) async* {
    // perhaps a more advanced method needs to be used here, maybe a popup
    //informing the user why the alarm couldn't be set
    yield PedometerAlarmError(error: error);
  }

  void onBackgroundFetch(String taskId) async {
    await localNotificationHandler.initNotificationManager();
    final goal = await pedometerRepository.getDailyGoal();
    final pedometerData = await pedometerRepository.getSavedNumberOfSteps();

    NotificationChannel channel =
        localNotificationHandler.scheduledNotificationChannels.last;
    customNotif.Notification notification = customNotif.Notification(
        channel: channel,
        title: "Daily goal reminder",
        id: 0,
        body:
            "Only  ${goal - pedometerData.steps} steps until you reach your goal. You can do it !");

    // Uncomment this for the demo -> a notification will be shown
	// without the time-related logic
	//localNotificationHandler.showNotification(notification);
    if (pedometerData.steps < goal) {
      // I think of this more like a server-side variable, so perhaps a repository
      // would be nice, if we'd decide to go with the hybrid approach of
      // FCM notifications + local notifications
      final sharedPrefs = await SharedPreferences.getInstance();
      final isNotificationScheduled = sharedPrefs
              .getBool(SharedPrefsKeys.isDailyGoalNotificationScheduled) ??
          false;

      if (isNotificationScheduled == false) {
        await localNotificationHandler.initNotificationManager();
        localNotificationHandler.scheduleNotification(
            notification, TimeZoneUtils.nextInstanceOf8PM());
        localNotificationHandler.showNotification(notification);
        sharedPrefs.setBool(
            SharedPrefsKeys.isDailyGoalNotificationScheduled, true);
        BackgroundFetch.finish(taskId);
      }
    } else {
      localNotificationHandler.cancelNotification(notification);
    }
  }
}
