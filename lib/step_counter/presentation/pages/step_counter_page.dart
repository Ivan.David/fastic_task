import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:fastic_task/step_counter/data/models/pedometer_data.dart';
import 'package:fastic_task/step_counter/presentation/bloc/pedometer_bloc.dart';
import 'package:fastic_task/step_counter/presentation/widgets/circular_progress_with_text.dart';
import 'package:fastic_task/step_counter/presentation/widgets/informative_column.dart';
import 'package:fastic_task/step_counter/presentation/widgets/linear_progress_bar_with_icon.dart';
import 'package:fastic_task/utils/custom_icons/custom_icons.dart';
import 'package:fastic_task/utils/fastic_colors.dart';
import 'package:fastic_task/utils/widgets/rounded_button.dart';
import "package:flutter_feather_icons/flutter_feather_icons.dart";
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StepCounterPage extends StatelessWidget {
  final PedometerData pedometerData;

  const StepCounterPage({Key key, @required this.pedometerData})
      : assert(pedometerData != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    // this should be closed by the blocProvider above him in the widget tree
    final pedometerBloc = BlocProvider.of<PedometerBloc>(context);
    final screenSize = MediaQuery.of(context).size;
    // min here in order not to overflow the charts if the user
    // exceeds his daily goal
    final percentage = min(pedometerData.steps / pedometerBloc.dailyGoal, 1.0);
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Stepcounter",
              style: TextStyle(
                  fontSize: 28,
                  color: FasticColors.darkBlue,
                  fontWeight: FontWeight.w900),
            ),
            SizedBox(
              height: 70,
            ),
            Center(
              child: CircularProgressWithText(
                percent: percentage,
                radius: screenSize.height / 4,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InformativeColumn(
                    icon: ImageIcon(
                      AssetImage('assets/steps.png'),
                      color: FasticColors.orange,
                    ),
                    title:
                        "${pedometerData.steps} / ${pedometerBloc.dailyGoal}",
                    // those texts would be localized in a prod app
                    subtitle: "Steps",
                  ),
                  InformativeColumn(
                    icon: ImageIcon(
                      AssetImage('assets/flame.png'),
                      color: FasticColors.orange,
                    ),
                    title: "${pedometerData.calories.floor()}",
                    // those texts would be localized in a prod app
                    subtitle: "Calories",
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: SizedBox(
                // half the width of the circular progress bar
                // or 100 in order not to overflow
                width: max(screenSize.height / 6, 100),
                child: RoundedButton(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          FeatherIcons.edit2,
                          size: 16,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        // localized as well
                        Flexible(
                            child: AutoSizeText(
                          "Daily Goal",
                          maxLines: 1,
                          minFontSize: 5,
                        )),
                      ]),
                  onPressed: () => pedometerBloc..add(ChangeDailyGoal()),
                ),
              ),
            ),
            // It seems like a good idea to compute this height dynamically
            // so we won't have a lot of unused space at the bottom of
            // longer screens
            SizedBox(
              height: screenSize.height / 10,
            ),
            LinearProgressBarWithIcon(
              icon: Transform(
                alignment: Alignment.center,
                transform: Matrix4.rotationY(pi),
                child: Icon(
                  CustomIcons.flag,
                  color: FasticColors.darkBlue,
                ),
              ),
              percent: percentage,
              width: screenSize.width * 0.9,
            )
          ],
        ),
      ),
    );
  }
}
