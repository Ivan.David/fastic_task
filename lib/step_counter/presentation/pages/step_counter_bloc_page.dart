import 'package:fastic_task/step_counter/presentation/bloc/pedometer_bloc.dart';
import 'package:fastic_task/step_counter/presentation/pages/step_counter_page.dart';
import 'package:fastic_task/step_counter/presentation/widgets/pedometer_alarm_button.dart';
import 'package:fastic_task/utils/fastic_colors.dart';
import 'package:fastic_task/utils/widgets/fastic_app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../injection_container.dart';

class StepCounterBlocPage extends StatefulWidget {
  @override
  _StepCounterBlocPageState createState() => _StepCounterBlocPageState();
}

class _StepCounterBlocPageState extends State<StepCounterBlocPage> {
  TextEditingController _controller;

  // instantiated here instead of in blocProvider because we need it
  // in text editing controller as well
  PedometerBloc _pedometerBloc;

  @override
  void initState() {
    super.initState();
    _pedometerBloc = serviceLocator<PedometerBloc>();
  }

  @override
  void dispose() {
    super.dispose();
    _pedometerBloc.close();
    if (_controller != null) {
      _controller.dispose();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: SafeArea(
            child: Scaffold(
          // TODO:- replace with alarm selection widget, which contains a bloc as well
          appBar: FasticAppBar(
            actions: [
              PedometerAlarmButton(),
            ],
          ),
          body: BlocProvider(
            create: (_) => _pedometerBloc..add(GetInitialData()),
            child: BlocListener<PedometerBloc, PedometerState>(
              listener: (context, state) {
                if (state is ChangingDailyGoal) {
                  if (_controller == null) {
                    _controller = TextEditingController();
                  }
                  // controller initial text = daily goal din bloc
                  //_controller.

                  _displayTextInputDialog(context, _controller,
                      "Change daily steps goal", "steps...", () {
                    // this can't be null because the text input type
                    // of the alert controller is number and the validator
                    // doesn't allow double numbers
                    Navigator.of(context).pop();
                    final newDailyGoal = int.tryParse(_controller.text);
                    _pedometerBloc
                        .add(DailyGoalChanged(dailyGoal: newDailyGoal));
                  });
                }
              },
              child: BlocBuilder<PedometerBloc, PedometerState>(
                  buildWhen: (previousState, nextState) {
                // I think is the 'cost' of having the daily goal inside
                // the same bloc as the pedometer data
                if (nextState is ChangingDailyGoal) {
                  return false;
                }
                return true;
              }, builder: (context, state) {
                if (state is StepDataChanged) {
                  return StepCounterPage(pedometerData: state.data);
                } else if (state is PedometerInitial) {
                  // this or show a cute little placeholder page (
                  // perhaps with a motivational message) when no steps
                  // are recorded for the current day
					return CircularProgressIndicator();
                } else if (state is PedometerError) {
                  return Center(
                    child: Text(state.error),
                  );
                }

                return Center(
                  child: CircularProgressIndicator(),
                );
              }),
            ),
          ),
        )));
  }

  // Perhaps this could be e separate util Widget, 'DoubleOptionAlertDialog',
  // but I feel like it could lead to too much generalization, since the alert
  // dialogs tend to be quite specific. However, it's an option worth considering
  // if there are lots of 2 optioned dialogs in the app
  Future<void> _displayTextInputDialog(
    BuildContext context,
    TextEditingController controller,
    String title,
    String hintText,
    VoidCallback okButtonPressed,
  ) async {
    final _formKey = GlobalKey<FormState>();
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Row(
              children: [
                Expanded(
                  child: Form(
                    key: _formKey,
                    child: TextFormField(
                      validator: (String value) {
                        if (value.contains(".") || value.startsWith("0")) {
                          return "Please enter a valid number";
                        }

                        return null;
                      },
                      keyboardType: TextInputType.number,
                      controller: controller,
                      decoration: InputDecoration(hintText: hintText),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text("steps"),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                color: FasticColors.softBlue,
                textColor: Colors.white,
                child: Text('CANCEL'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                color: FasticColors.orange,
                textColor: Colors.white,
                child: Text('OK'),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    okButtonPressed();
                  }
                },
              ),
            ],
          );
        });
  }
}
