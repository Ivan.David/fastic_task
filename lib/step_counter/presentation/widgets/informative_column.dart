import 'package:auto_size_text/auto_size_text.dart';
import 'package:fastic_task/utils/fastic_colors.dart';
import 'package:flutter/cupertino.dart';

// TODO:- come up with a better name
// think this is not so informative, so I could use some suggestions here
class InformativeColumn extends StatelessWidget {
  final ImageIcon icon;
  final String title;
  final String subtitle;

  const InformativeColumn(
      {Key key, @required this.icon, @required this.title, this.subtitle = ''})
      : assert(icon != null),
        assert(title != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        icon,
        SizedBox(
          height: 4,
        ),
        AutoSizeText(title,
            style: TextStyle(
                fontSize: 14,
                color: FasticColors.softBlue,
                fontWeight: FontWeight.bold)),
        SizedBox(
          height: 4,
        ),
        AutoSizeText(subtitle,
            style: TextStyle(fontSize: 14, color: FasticColors.softBlue))
      ],
    );
  }
}
