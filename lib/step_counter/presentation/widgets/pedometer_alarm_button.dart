import 'package:fastic_task/step_counter/presentation/bloc/pedometer_alarm_bloc.dart';
import 'package:fastic_task/utils/fastic_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../injection_container.dart';

class PedometerAlarmButton extends StatefulWidget {
  @override
  _PedometerAlarmButtonState createState() => _PedometerAlarmButtonState();
}

class _PedometerAlarmButtonState extends State<PedometerAlarmButton> {
  PedometerAlarmBloc alarmBloc;

  @override
  void initState() {
    super.initState();
    alarmBloc = serviceLocator<PedometerAlarmBloc>();
  }

  @override
  void dispose() {
    alarmBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PedometerAlarmBloc>(
      create: (_) => alarmBloc,
      child: BlocListener<PedometerAlarmBloc, PedometerAlarmState>(
        listener: (context, state) {
          if (state is PedometerAlarmOn) {
            // remove any previously existing snackBar
            Scaffold.of(context).removeCurrentSnackBar();
            Scaffold.of(context).showSnackBar(
              SnackBar(
                backgroundColor: FasticColors.darkBlue,
                content: Text(
                    'You will be reminded at 8 PM if your daily goal isn\'t met by then'),
              ),
            );
          }
        },
        child: BlocListener<PedometerAlarmBloc, PedometerAlarmState>(
          listener: (context, state) {
            if (state is PedometerAlarmError) {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: FasticColors.darkBlue,
                  content: Text(
                      'There was an error while setting up the alarm: ${state.error}'),
                ),
              );
            }
          },
          child: BlocBuilder<PedometerAlarmBloc, PedometerAlarmState>(
            builder: (context, state) {
              if (state is PedometerAlarmError) {
                return Icon(
                  Icons.close,
                  color: FasticColors.darkBlue,
                );
              } else if (state is PedometerAlarmOn ||
                  state is PedometerAlarmOff) {
                return IconButton(
                    icon: state is PedometerAlarmOn
                        ? Icon(
                            Icons.notifications_off_outlined,
                            color: FasticColors.darkBlue,
                          )
                        : Icon(
                            Icons.add_alert_outlined,
                            color: FasticColors.darkBlue,
                          ),
                    onPressed: () => alarmBloc..add(AlarmBellPressed()));
              } else {
                // pedometer Loading
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
