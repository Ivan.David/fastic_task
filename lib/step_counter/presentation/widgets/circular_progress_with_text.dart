import 'package:fastic_task/utils/fastic_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

// this can be made more general, with the colors extracted in the constructor if needed
// but for now I've considered it as a feature-dependent widget
class CircularProgressWithText extends StatelessWidget {
  final double percent;
  final double radius;

  CircularProgressWithText({@required this.percent, @required this.radius})
      : assert(percent != null),
        assert(radius != null);

  @override
  Widget build(BuildContext context) {
    return CircularPercentIndicator(
      circularStrokeCap: CircularStrokeCap.round,
      animation: true,
      radius: radius,
      lineWidth: 10.0,
      percent: percent,
      center: Text(
        "${(percent * 100).toStringAsFixed(0)}%",
        style: TextStyle(
            fontSize: 45,
            fontWeight: FontWeight.bold,
            color: FasticColors.darkBlue),
      ),
      progressColor: FasticColors.orange,
      backgroundColor: FasticColors.fadeGray,
    );
  }
}
