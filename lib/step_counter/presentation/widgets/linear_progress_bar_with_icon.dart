import 'package:fastic_task/utils/fastic_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class LinearProgressBarWithIcon extends StatelessWidget {
  final double percent;
  final Widget icon;
  final double width;

  const LinearProgressBarWithIcon(
      {Key key, @required this.percent, @required this.width, this.icon})
      : assert(percent != null),
        assert(width != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final linearIndicator = LinearPercentIndicator(
      width: width,
      lineHeight: 8.0,
      percent: percent,
      progressColor: FasticColors.orange,
      backgroundColor: FasticColors.fadeGray,
      animation: true,
    );

    return icon == null
        ? linearIndicator
        : Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              icon,
              linearIndicator
            ],
          );
  }
}
