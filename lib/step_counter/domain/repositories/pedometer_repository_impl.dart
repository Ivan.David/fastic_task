import 'package:dartz/dartz.dart';
import 'package:fastic_task/step_counter/data/datasources/pedometer_data_source.dart';
import 'package:fastic_task/step_counter/data/datasources/pedometer_local_data_source.dart';
import 'package:fastic_task/step_counter/data/models/pedometer_data.dart';
import 'package:fastic_task/step_counter/domain/repositories/pedometer_repository.dart';
import 'package:fastic_task/utils/failures.dart';
import 'package:flutter/widgets.dart';

class PedometerRepositoryImpl implements PedometerRepository {
  final PedometerDataSource pedometerDataSource;
  final PedometerLocalDataSource pedometerLocalDataSource;

  PedometerRepositoryImpl(
      {@required this.pedometerDataSource,
      @required this.pedometerLocalDataSource})
      : assert(pedometerDataSource != null),
        assert(pedometerLocalDataSource != null);

  @override
  Either<Failure, Stream<PedometerData>> getCurrentNoOfSteps() {
    final stepsStream = pedometerDataSource.getCurrentNoOfSteps();

    if (stepsStream == null) {
      return Left(
          StreamFailure(error: "Unknown error while retrieving step data"));
    }

    String error;
    stepsStream.handleError((streamError) {
      print(streamError);
      error = streamError;
    });

    if (error != null) {
      return Left(StreamFailure(error: error));
    }

    return Right(stepsStream);
  }

  @override
  Future<PedometerData> getSavedNumberOfSteps() async {
    return await pedometerLocalDataSource.getCurrentNoOfSteps();
  }

  @override
  Future saveCurrentNoOfSteps(PedometerData data) async {
    return await pedometerLocalDataSource.saveCurrentNoOfSteps(data);
  }

  @override
  Future<int> getDailyGoal() async {
    return await pedometerLocalDataSource.getDailyGoal();
  }

  @override
  Future saveDailyGoal(int dailyGoal) async {
    return await pedometerLocalDataSource.saveDailyGoal(dailyGoal);
  }
}
