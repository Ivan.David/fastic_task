import 'package:dartz/dartz.dart';
import 'package:fastic_task/step_counter/data/models/pedometer_data.dart';
import 'package:fastic_task/utils/failures.dart';

// I find this dartz plugin as very useful in increasing readability
// and providing a clear way to identify errors
abstract class PedometerRepository {
  Either<Failure, Stream<PedometerData>> getCurrentNoOfSteps();
  Future saveCurrentNoOfSteps(PedometerData data);
  // Perhaps this should be inside the getCurrentNoOfSteps method, with helper
  // method inside the repo specifying whether the data should be fetched
  // locally or not
  Future<PedometerData> getSavedNumberOfSteps();
  Future saveDailyGoal(int dailyGoal);
  Future<int> getDailyGoal();
}
