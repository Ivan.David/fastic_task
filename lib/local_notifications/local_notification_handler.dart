// I think this is one way of doing things regarding the notification user story
// This way has the advantage of working offline but the disadvantage that the
// documentation says that it won't work on iOS if the app is terminated
// and that there is no guarantee that the background fetch task will execute
// For a production app, I would propose a hybrid approach depending on whether
// the use has an internet connection or not, with a firebase FCM notification
// being sent via a cloud function if the user hasn't reached his goal.
// Of course, this means that the user data would have to be stored in
// cloud along a push notification token as well (Cloud Firestore or RealtimeDB)
import 'dart:typed_data';

import 'package:fastic_task/local_notifications/models/notification.dart';
import 'package:fastic_task/local_notifications/models/notification_channel.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class LocalNotificationHandler {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  IOSInitializationSettings initializationSettingsIOS;
  InitializationSettings initializationSettings;

  final List<NotificationChannel> scheduledNotificationChannels = [
    NotificationChannel(
        id: "daily_goal_reminder",
        description: "Daily goal reminder",
        name: "Daily Goal reminder",
        soundName: "",
        hasSound: false),
  ];

  Future initNotificationManager() async {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    initializationSettingsIOS = IOSInitializationSettings();
    initializationSettings =
        InitializationSettings(iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  Future<void> scheduleNotification(
      Notification notification, tz.TZDateTime scheduledTime) async {
    var vibrationPattern = Int64List(4);
    vibrationPattern[0] = 0;
    vibrationPattern[1] = 1000;
    vibrationPattern[2] = 5000;
    vibrationPattern[3] = 2000;

    await flutterLocalNotificationsPlugin.zonedSchedule(
        0,
        notification.title,
        notification.body,
        scheduledTime,
        NotificationDetails(
            android: AndroidNotificationDetails(notification.channel.id,
                notification.channel.name, notification.channel.description)),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime);
  }

  Future<void> showNotification(Notification notification) async {
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics =
        NotificationDetails(iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, notification.title, notification.body, platformChannelSpecifics,
        payload: 'item x');
  }

  Future<void> cancelNotification(Notification notification) async {
    await flutterLocalNotificationsPlugin.cancel(notification.id);
  }
}
