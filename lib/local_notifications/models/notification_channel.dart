import 'package:flutter/widgets.dart';

class NotificationChannel {
  final String id;
  final String name;
  final String description;
  final String soundName;
  final bool hasSound;

  NotificationChannel(
      {@required this.id,
      @required this.name,
      @required this.description,
      this.soundName,
      this.hasSound})
      : assert(id != null),
        assert(name != null),
        assert(description != null);
}
