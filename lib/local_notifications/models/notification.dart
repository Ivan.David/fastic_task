import 'package:fastic_task/local_notifications/models/notification_channel.dart';
import 'package:flutter/widgets.dart';

class Notification {
  final int id;
  final String title;
  final String body;
  final NotificationChannel channel;

  Notification(
      {@required this.id,
      @required this.title,
      @required this.body,
      @required this.channel})
      : assert(id != null),
        assert(title != null),
        assert(body != null),
        assert(channel != null);
}
