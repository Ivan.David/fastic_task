import 'package:fastic_task/step_counter/presentation/pages/step_counter_bloc_page.dart';
import 'package:fastic_task/utils/background_fetch_handler.dart';
import 'package:fastic_task/utils/fastic_colors.dart';
import 'package:fastic_task/utils/shared_prefs_keys.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

import 'injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  tz.initializeTimeZones();
  await di.init();
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('font/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });

  // set up initial default values for goal and daily steps
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setInt(SharedPrefsKeys.dailyGoalKey, 10000);
  prefs.setInt(SharedPrefsKeys.numberOfSteps, 0);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fastic Task',
      theme: ThemeData(
        textTheme: GoogleFonts.latoTextTheme(
          Theme.of(context).textTheme,
        ),
        primaryColor: Colors.white,
        buttonColor: FasticColors.fadeGray,
        accentColor: FasticColors.gray,
        appBarTheme: AppBarTheme(
            elevation: 0, // This removes the shadow from all App Bars.
            shadowColor: Colors.white
            ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: StepCounterBlocPage(),
    );
  }
}
